import * as React from 'react';
import Box from '@mui/material/Box';
import { DataGrid } from '@mui/x-data-grid';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { useNavigate } from "react-router-dom";
import Pluralize from 'pluralize';
import { Paper } from '@mui/material';
import { Card } from '@mui/material';

export default function DataGridD({ columns, rows, entity, createUrl = false }) {
  let navigate = useNavigate();
  const capitalize = s => s && s[0].toUpperCase() + s.slice(1)

  return (
    <>

      <Box sx={{ height: 400, width: '80%' }}>


        <Box sx={{ display: 'flex', py: '25px' }}>
          <Typography variant="h6" component="h2" color="primary" sx={{ flexGrow: '1' }}>
            {Pluralize(capitalize(entity))}
          </Typography>
          {createUrl ? <Button
            variant="outlined"
            color="secondary"
            onClick={() => {
              navigate(createUrl)
            }}
          // startIcon={<PersonAddIcon />}
          >
            Add New
          </Button> : ''}

        </Box>
        <DataGrid
          sx={{
            boxShadow: 2, align: 'center'
          }}
          rows={rows}
          columns={columns}
          pageSize={5}
          rowsPerPageOptions={[5]}
          checkboxSelection
          disableSelectionOnClick

        />
      </Box>

    </>
  );
}