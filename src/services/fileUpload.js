import axios from "axios";


const endpoint = process.env.REACT_APP_API_URL + `/file-upload`;


const before = interceptor => {
    return axios.interceptors.request.use(interceptor);
};

const after = interceptor => {
    return axios.interceptors.response.use(interceptor);
};

const index = () => {
    const res = axios.get(endpoint);
    return res;
};

const create = (data) => {
    const res = axios.post(endpoint, data);
    return res;
};

const update = (id, data) => {
    const res = axios.get(endpoint);
    return res;
};

const show = slug => {
    const res = axios.get(endpoint + '/' + slug);
    return res;
};

const destroy = slug => {
    const res = axios.get(endpoint + '/' + slug);
    return res;
};



const all = {
    index,
    create,
    update,
    destroy,
    show,
    before,
    after
};

export default all;