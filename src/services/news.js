import axios from "axios";


const endpoint = process.env.REACT_APP_API_URL + `/news`;


const before = interceptor => {
    return axios.interceptors.request.use(interceptor);
};

const after = interceptor => {
    return axios.interceptors.response.use(interceptor);
};

const index = () => {
    const res = axios.get(endpoint + '/');
    return res;
};

const create = (data) => {
    const res = axios.post(endpoint, data);
    return res;
};

const update = (id, data) => {
    const res = axios.patch(endpoint + '/' + id, data);
    return res;
};

const show = id => {
    const res = axios.get(endpoint + '/' + id);
    return res;
};

const destroy = id => {
    const res = axios.delete(endpoint + '/' + id);
    return res;
};

const isUnique = name => {
    const res = axios.get(`${endpoint}/is-unique/${name}`);
    return res;
};



const all = {
    index,
    create,
    update,
    destroy,
    show,
    before,
    after,
    isUnique
};

export default all;
