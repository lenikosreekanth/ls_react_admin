import axios from "axios";
// const API_URL = "http://localhost:3000/auth/login";

const API_URL = process.env.REACT_APP_API_URL + `/auth/`;

const register = (username, email, password) => {
    return axios.post(API_URL + "signup", {
        username,
        email,
        password,
    });
};

const login = (data) => {
    return axios
        .post(API_URL + "login", data)
        .then((response) => {

            // console.log(response.data)
            if (response.data.access_token) {
                
                localStorage.setItem("user", JSON.stringify(response.data));
            }
            return response.data;
        });
};
const logout = () => {
    localStorage.removeItem("user");
};
const getCurrentUser = () => {
    return JSON.parse(localStorage.getItem("user"));
};
const AuthService = {
    register,
    login,
    logout,
    getCurrentUser,
};
export default AuthService;