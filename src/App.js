import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import AuthService from './services/auth.service';
import { useEffect, useState } from 'react';
import Layout from './components/Layout';
import DashBoard from './pages/DashBoard';
import BlogForm from './pages/Blogs/Form';
import BlogIndex from './pages/Blogs/Index';
import GalleryForm from './pages/Gallery/Form';
import GalleryIndex from './pages/Gallery/Index';
import PhotosIndex from './pages/Gallery/Photos/Index';
import PhotosForm from './pages/Gallery/Photos/Form';
import NewsIndex from './pages/News/Index';
import NewsForm from './pages/News/Form';
import Login from './pages/Auth/Login';
import CsrForm from './pages/Csr/Form';
import CsrIndex from './pages/Csr/Index';
import EnquiryIndex from './pages/Enquiry/Index';






function App() {

  const [currentUser, setCurrentUser] = useState(undefined);

  useEffect(() => {
    const user = AuthService.getCurrentUser();
    if (user) {
      setCurrentUser(user);
    }
  }, []);


  return (
    <>
      <BrowserRouter>

        <Routes>
          <Route path="auth/login" element={<Login />} />
        </Routes>

        {currentUser ? <Layout>
          <Routes>
            <Route exact path="/dashboard" element={<DashBoard />} />

            <Route path="blogs" element={<BlogIndex />} />
            <Route path="blogs/:id" element={<BlogForm />} />
            <Route path="blogs/create" element={<BlogForm />} />

            <Route path="gallery" element={<GalleryIndex />} />
            <Route path="gallery/:id" element={<GalleryForm />} />
            <Route path="gallery/create" element={<GalleryForm />} />

            <Route path="photos" element={<PhotosIndex />} />
            <Route path="photos/:id" element={<PhotosForm />} />
            <Route path="photos/create" element={<PhotosForm />} />

            <Route path="news-and-events" element={<NewsIndex />} />
            <Route path="news-and-events/:id" element={<NewsForm />} />
            <Route path="news-and-events/create" element={<NewsForm />} />

            <Route path="csr" element={<CsrIndex />} />
            <Route path="csr/:id" element={<CsrForm />} />
            <Route path="csr/create" element={<CsrForm />} />

            <Route path="enquiry" element={<EnquiryIndex />} />


          </Routes>
        </Layout> : ''}
        <Routes>
          <Route path="*" />
        </Routes>

      </BrowserRouter>

    </>
  );
}

export default App;