import { useEffect } from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import csrService from '../../../services/csr';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useNavigate, useParams } from "react-router-dom";
import ImageUpload from '../../../components/ImageUpload';

import { Paper, FormControl, Select, MenuItem, InputLabel } from '@mui/material';

const theme = createTheme();

export default function SignUp() {
    const { id } = useParams();

    useEffect(() => {
        if (id) {
            fetchData();
        }
    }, []);


    const fetchData = () => {
        csrService.show(id).then(blog => {
            formik.setValues(blog.data);
        }).catch(err => { })
    }

    let navigate = useNavigate();

    const formik = useFormik({
        initialValues: {
            name: '',
            short_description: '',
            description: '',
            image: '',
            is_active: true,
            responsibility_type: 1
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .max(15, 'Must be 15 characters or less')
                .required('Required'),
            // .test("unique", "Name must be unique",
            //     async (value) => {
            //         const { data: res } = await csrService.isUnique(value)
            //         return res;
            //     }),
            short_description: Yup.string()
                .max(1000, 'Must be 1000 characters or less')
                .required('Required'),
            description: Yup.string()
                .max(5000, 'Must be 5000 characters or less')
                .required('Required'),
            is_active: Yup.boolean(),
            image: Yup.string()
                .required('Required'),
        }),

        onSubmit: values => {

            if (id) {
                csrService.update(id, values).then(res => {
                }).catch(err => { })

            } else {
                csrService.create(values).then(res => {
                }).catch(err => { })
            }
            return navigate("/csr");
        },

    });


    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="lg">
                <CssBaseline />
                <Box
                    sx={{
                        padding: 3,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'left',
                    }}
                >
                    <Typography component="h1" variant="h5" sx={{ px: 5, py: 2 }}>
                        Csr {id ? 'Edit' : 'Create'}
                    </Typography>
                    <Paper elevation={3} sx={{ mx: 5 }}>
                        <Box component="form" noValidate onSubmit={formik.handleSubmit} sx={{ m: 8 }}>
                            <Grid container spacing={2}>
                                <Grid item xs={12} sm={12}>
                                    <TextField
                                        autoComplete={false}
                                        name="name"
                                        required
                                        fullWidth
                                        id="name"
                                        label="Name"
                                        onChange={formik.handleChange}
                                        value={formik.values.name}
                                        onBlur={formik.handleBlur}
                                    />

                                    {formik.touched.name && formik.errors.name ? (
                                        <Box sx={{ color: 'red' }}>{formik.errors.name}</Box>
                                    ) : null}
                                </Grid>
                                <Grid item xs={12} sm={12}>
                                    <FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-label">Type</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            label="Type"
                                            name="responsibility_type"
                                            onChange={formik.handleChange}
                                            value={formik.values.responsibility_type}
                                            onBlur={formik.handleBlur}
                                        >
                                            <MenuItem selected={true} value={1}>Social</MenuItem>
                                            <MenuItem value={2}>CSR</MenuItem>
                                        </Select>
                                    </FormControl>
                                    {formik.touched.responsibility_type && formik.errors.responsibility_type ? (
                                        <Box sx={{ color: 'red' }}>{formik.errors.responsibility_type}</Box>
                                    ) : null}
                                </Grid>


                                <Grid item sm={12}>
                                    <TextField
                                        id="filled-multiline-static"
                                        label="Short Description"
                                        multiline
                                        rows={4}
                                        fullWidth
                                        name="short_description"
                                        onChange={formik.handleChange}
                                        value={formik.values.short_description}
                                        onBlur={formik.handleBlur}
                                    />
                                    {formik.touched.short_description && formik.errors.short_description ? (
                                        <Box sx={{ color: 'red' }}>{formik.errors.short_description}</Box>
                                    ) : null}
                                </Grid>

                                <Grid item sm={12}>
                                    <TextField
                                        id="filled-multiline-static"
                                        label="Description"
                                        multiline
                                        rows={4}
                                        fullWidth
                                        name="description"
                                        onChange={formik.handleChange}
                                        value={formik.values.description}
                                        onBlur={formik.handleBlur}
                                    />

                                    {formik.touched.description && formik.errors.description ? (
                                        <Box sx={{ color: 'red' }}>{formik.errors.description}</Box>
                                    ) : null}
                                </Grid>


                                <Grid item xs={12}>
                                    <ImageUpload
                                        name='image'
                                        entity='csr'
                                        formik={formik}
                                    />
                                </Grid>

                                <Grid item xs={12}>
                                    <FormControlLabel
                                        control={<Checkbox
                                            name="is_active"
                                            checked={formik.values.is_active}
                                            color="primary"
                                            value={formik.values.is_active}
                                            onChange={() => formik.setFieldValue('is_active', !formik.values.is_active)}
                                        />}
                                        label="Status"
                                    />
                                </Grid>

                            </Grid>
                            <Button
                                type="submit"
                                // fullWidth
                                variant="contained"
                                sx={{ mt: 3, mb: 2 }}
                            >
                                {id ? 'Update' : 'Submit'}
                            </Button>

                            <Button
                                type="submit"
                                // fullWidth
                                color="warning"
                                variant="contained"
                                sx={{ mt: 3, mb: 2, ml: 2 }}
                                onClick={() => navigate("/csr")}
                            >
                                Cancel
                            </Button>

                        </Box>
                    </Paper>
                </Box>

            </Container>
        </ThemeProvider>
    );
}