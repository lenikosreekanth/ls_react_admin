import { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import GalleryService from '../../../services/gallery';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useNavigate, useParams } from "react-router-dom";
import ImageUpload from '../../../components/ImageUpload';
import { Paper } from '@mui/material';


const theme = createTheme();

export default function SignUp() {
    const { id } = useParams();

    const [data, setData] = useState({});

    useEffect(() => {
        if (id) {
            fetchData()
        }
    }, []);


    const fetchData = () => {
        GalleryService.show(id).then(data => {
            setData(data.data);
            formik.setValues(data.data);
        }).catch(err => { })
    }



    let navigate = useNavigate();

    const formik = useFormik({
        initialValues: {
            name: '',
            short_description: '',
            image: '',
            is_active: true,
        },

        validationSchema: Yup.object({
            name: Yup.string()
                .max(15, 'Must be 15 characters or less')
                .required('Required')
                // .when('sss', (email, schema, val) => {
                //     return !email && !val;
                // }
                // )
                .when('short_description', (short_description, field) =>{
                    //  field.required();
                    console.log(field);
                    
                }
    ),
            // .test("unique", "Name must be unique",
            //     async (value) => {
            //         const { data: res } = await GalleryService.isUnique(value)
            //         return res;
            //     })
            
            short_description: Yup.string()
                .max(1000, 'Must be 1000 characters or less')
                .required('Required'),
            is_active: Yup.boolean(),
            image: Yup.string()
                .required('Required'),

        }),

        onSubmit: values => {
            if (id) {
                if (data !== values) {
                    GalleryService.update(id, values)
                }

            } else {
                GalleryService.create(values);
            }
            return navigate("/gallery");
        },

    });


    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="lg">
                <CssBaseline />
                <Box
                    sx={{
                      
                        padding: 3,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'left',
                    }}
                >
                    <Typography component="h1" variant="h5"  sx={{px:5,py:2}}>
                        Gallery {id ? 'Edit' : 'Create'}
                    </Typography>
                    <Paper elevation={3} sx={{mx: 5 }}>
                    <Box component="form" noValidate onSubmit={formik.handleSubmit} sx={{ m: 8 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={12}>
                                <TextField
                                    autoComplete={false}
                                    name="name"
                                    required
                                    fullWidth
                                    id="name"
                                    label="Name"
                                   
                                    onChange={formik.handleChange}
                                    value={formik.values.name}
                                    onBlur={formik.handleBlur}
                                />

                                {formik.touched.name && formik.errors.name ? (
                                    <Box sx={{color:'red'}}>{formik.errors.name}</Box>
                                ) : null}
                            </Grid>

                            <Grid item sm={12}>
                                <TextField
                                    id="filled-multiline-static"
                                    label="Short Description"
                                    multiline
                                    rows={4}
                                    fullWidth
                                    name="short_description"
                                   
                                    onChange={formik.handleChange}
                                    value={formik.values.short_description}
                                    onBlur={formik.handleBlur}
                                />

                                {formik.touched.short_description && formik.errors.short_description ? (
                                   <Box sx={{color:'red'}}>{formik.errors.short_description}</Box>
                                ) : null}
                            </Grid>

                            <Grid item xs={12}>
                                <ImageUpload
                                    name='image'
                                    entity='gallery'
                                    formik={formik}
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <FormControlLabel
                                    control={<Checkbox value="allowExtraEmails" color="primary" />}
                                    label="Status"
                                />

                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            // fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            {id ? 'Update' : 'Submit'}
                        </Button>

                        <Button
                            type="submit"
                            // fullWidth
                            color="warning"
                            variant="contained"
                            sx={{ mt: 3, mb: 2, ml: 2 }}
                        >
                            Cancel
                        </Button>

                    </Box>
                    </Paper>
                </Box>

            </Container>
        </ThemeProvider>
    );
}