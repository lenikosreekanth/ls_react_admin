import { useEffect, useState } from 'react';
import Button from '@mui/material/Button';
import CssBaseline from '@mui/material/CssBaseline';
import TextField from '@mui/material/TextField';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import Grid from '@mui/material/Grid';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import PhotosService from '../../../../services/photos';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { useNavigate, useParams } from "react-router-dom";
import ImageUpload from '../../../../components/ImageUpload';
import GalleryService from '../../../../services/gallery';
import MenuItem from '@mui/material/MenuItem';
import { Paper } from '@mui/material';


const theme = createTheme();

export default function SignUp() {
    const { id } = useParams();
    let navigate = useNavigate();

    const [galleries, setGalleries] = useState([]);

    useEffect(() => {
        fetchGalleries()
        if (id) {
            fetchData()
        }
    }, []);



    const fetchData = () => {
        PhotosService.show(id).then(data => {
            formik.setValues(data.data);
        }).catch(err => { })
    }

    const fetchGalleries = () => {
        GalleryService.index().then(data => {
            setGalleries(data.data.items);
        }).catch(err => { })
    }


    const formik = useFormik({
        initialValues: {
            name: '',
            gallery_id: '',
            image: '',
            is_active: true,
        },
        validationSchema: Yup.object({
            name: Yup.string()
                .max(15, 'Must be 15 characters or less')
                .required('Required'),
            is_active: Yup.boolean(),
            image: Yup.string()
                .required('Required'),

        }),

        onSubmit: values => {
            if (id) {
                PhotosService.update(id, values).then(res => {
                })

            } else {
                PhotosService.create(values).then(res => {
                })
            }
            return navigate("/photos");
        },

    });


    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="lg">
                <CssBaseline />
                <Box
                    sx={{
                       
                        padding: 3,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'left',
                    }}
                >
                    <Typography component="h1" variant="h5"   sx={{px:5,py:2}}>
                        Photos {id ? 'Edit' : 'Create'}
                    </Typography>
                    <Paper elevation={3} sx={{mx: 5 }}>
                    <Box component="form" noValidate onSubmit={formik.handleSubmit} sx={{ m: 8 }}>
                        <Grid container spacing={2}>
                            <Grid item xs={6} sm={6}>
                                <TextField
                                    autoComplete={false}
                                    name="name"
                                    required
                                    fullWidth
                                    id="name"
                                    label="Name"
                                    
                                    onChange={formik.handleChange}
                                    value={formik.values.name}
                                    onBlur={formik.handleBlur}
                                />

                                {formik.touched.name && formik.errors.name ? (
                                  <Box sx={{color:'red'}}>{formik.errors.name}</Box>
                                ) : null}
                            </Grid>

                            <Grid item xs={6} sm={6}>
                                <TextField
                                    id="outlined-select-currency"
                                    select
                                    fullWidth
                                    label="Select"
                                    name="gallery_id"
                                    onChange={formik.handleChange}
                                    value={formik.values.gallery_id}
                                    onBlur={formik.handleBlur}
                                    helperText="Please select Gallery"
                                >
                                    {galleries.map((option) => (
                                        <MenuItem key={option.id} value={option.id}>
                                            {option.name}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </Grid>

                            <Grid item xs={12}>
                                <ImageUpload
                                    name='image'
                                    entity='photos'
                                    formik={formik}
                                />
                            </Grid>

                            <Grid item xs={12}>
                                <FormControlLabel
                                    control={<Checkbox value="allowExtraEmails" color="primary" />}
                                    label="Status"
                                />
                            </Grid>
                        </Grid>
                        <Button
                            type="submit"
                            // fullWidth
                            variant="contained"
                            sx={{ mt: 3, mb: 2 }}
                        >
                            {id ? 'Update' : 'Submit'}
                        </Button>

                        <Button
                            type="submit"
                            // fullWidth
                            color="warning"
                            variant="contained"
                            sx={{ mt: 3, mb: 2, ml: 2 }}
                        >
                            Cancel
                        </Button>

                    </Box>
                    </Paper>
                </Box>

            </Container>
        </ThemeProvider>
    );
}