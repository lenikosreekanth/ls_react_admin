import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from "react-router-dom";
import { useEffect, useState } from 'react';
import { Grid, Container, IconButton, Select, MenuItem, CssBaseline } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import EditIcon from '@mui/icons-material/Edit';
import swal from 'sweetalert';
import DataGrid from '../../../../components/DataGrid';
import photoService from '../../../../services/photos';
import galleryService from '../../../../services/gallery';
import Box from '@mui/material/Box';


const theme = createTheme();

export default function Index() {

    let navigate = useNavigate();
    const [data, setData] = useState([]);
    const [galleries, setGalleries] = useState([]);


    const thumbsContainer = {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 16
    };

    const thumb = {
        display: 'inline-flex',
        borderRadius: 2,
        border: '1px solid #eaeaea',
        marginBottom: 8,
        marginRight: 8,
        width: 100,
        height: 100,
        padding: 4,
        boxSizing: 'border-box'
    };

    const thumbInner = {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden'
    };

    const img = {
        display: 'block',
        width: 'auto',
        height: '100%'
    };


    const columns = [
        { field: 'id', headerName: 'ID', width: 90 },
        {
            field: 'name',
            headerName: 'Name',
            width: 150,
        },
        {
            field: 'image',
            headerName: 'Image',
            width: 150,
            renderCell: (cellValues) => {
                return (
                    <>
                        <aside style={thumbsContainer}>
                            <div style={thumb} key={cellValues.id}>
                                <div style={thumbInner}>
                                    <img
                                        src={`${process.env.REACT_APP_API_URL}/uploads/photos/${cellValues.row.image}`}
                                        style={img}
                                        alt="thumbnail"
                                    />
                                </div>
                            </div>
                        </aside>

                    </>
                );
            }
        },
        {
            headerName: 'Action',
            width: 150,
            renderCell: (cellValues) => {
                return (
                    <>
                        <IconButton aria-label="fingerprint" color="secondary">
                            <DeleteIcon

                                onClick={(event) => {
                                    handleDelete(cellValues.row.id);
                                }}
                            />
                        </IconButton>
                        <IconButton aria-label="fingerprint" color="secondary">
                            <EditIcon
                                fontSize="small"
                                onClick={(event) => {
                                    navigate('/photos/' + cellValues.row.id)
                                }}
                            />
                        </IconButton>
                    </>
                );
            }
        }
    ];



    const handleDelete = (id) => {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this item!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    photoService.destroy(id).then(() => {
                        navigate('/photos');
                    })

                    swal("Poof! Your item has been deleted!", {
                        icon: "success",
                    });
                } else {

                }
            });
    }


    useEffect(() => {
        fetchGalleries();
        fetchPhotos();
    }, [])

    const fetchGalleries = () => {
        galleryService.index().then(data => {
            setGalleries(data.data.items);
        }).catch(err => { })
    }

    const fetchPhotos = (galleryId = '') => {
        photoService.index(galleryId).then(res => {
            setData(res.data.items);
        }).catch(err => { })
    }

    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="lg">
                <CssBaseline />
                <Grid item xs={6} sm={6}>
                <Grid container>
                
                <Grid item xs={6} columnSpacing={2}>
                <Box mt={2}>
                    <Select
                        id="outlined-select-currency"
                        select
                        label="Select"
                        fullWidth
                     
                        name="gallery_id"
                        onChange={(event) => { fetchPhotos(event.target.value) }}
                        // helperText="Please select Gallery"
                        
                    >
                        {/* <MenuItem selected={true} value=''>All</MenuItem> */}
                        {galleries.map((option) => (
                            <MenuItem key={option.id} value={option.id}>
                                {option.name}
                            </MenuItem>
                        ))}
                    </Select>
                    </Box>
             
               </Grid>
                <DataGrid
                    columns={columns}
                    rows={data}
                    entity="photos"
                    createUrl="/photos/create"
                />
                 </Grid>
                </Grid>
                
            </Container>
        </ThemeProvider>
    );
}