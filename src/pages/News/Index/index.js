import { useEffect, useState } from 'react';
import CssBaseline from '@mui/material/CssBaseline';
import Container from '@mui/material/Container';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import { useNavigate } from "react-router-dom";
import newsService from '../../../services/news';
import swal from 'sweetalert';
import DataGrid from '../../../components/DataGrid';
import DeleteIcon from '@mui/icons-material/Delete';
import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';



const theme = createTheme();

export default function SignUp() {
    let navigate = useNavigate();

    const thumbsContainer = {
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        marginTop: 16
    };

    const thumb = {
        display: 'inline-flex',
        borderRadius: 2,
        border: '1px solid #eaeaea',
        marginBottom: 8,
        marginRight: 8,
        width: 100,
        height: 100,
        padding: 4,
        boxSizing: 'border-box'
    };

    const thumbInner = {
        display: 'flex',
        minWidth: 0,
        overflow: 'hidden'
    };

    const img = {
        display: 'block',
        width: 'auto',
        height: '100%'
    };

    const [data, setData] = useState([]);


    const columns = [
        { field: 'id', headerName: 'ID', width: 90 },
        {
            field: 'name',
            headerName: 'Name',
            width: 150,
        },
        {
            field: 'image',
            headerName: 'Image',
            width: 150,
            renderCell: (cellValues) => {
                return (
                    <>
                        <aside style={thumbsContainer}>
                            <div style={thumb} key={cellValues.id}>
                                <div style={thumbInner}>
                                    <img
                                        src={`${process.env.REACT_APP_API_URL}/uploads/news/${cellValues.row.image}`}
                                        style={img}
                                        alt="thumbnail"
                                    />
                                </div>
                            </div>
                        </aside>

                    </>
                );
            }
        },
        {
            headerName: 'Action',
            width: 150,
            renderCell: (cellValues) => {
                return (
                    <>
                        <IconButton aria-label="fingerprint" color="secondary">
                            <DeleteIcon
                                fontSize="small"
                                onClick={(event) => {
                                    handleDelete(cellValues.row.id);
                                }}
                            />
                        </IconButton>
                        <IconButton aria-label="fingerprint" color="secondary">
                            <EditIcon
                                fontSize="small"
                                onClick={(event) => {
                                    navigate('/news-and-events/' + cellValues.row.id)
                                }}
                            />
                        </IconButton>
                    </>
                );
            }
        }
    ];


    useEffect(() => {
        newsService.index().then(res => {
            setData(res.data.items);
        }).catch(err => { })
    }, [data])

    const handleDelete = (id) => {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this item!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        })
            .then((willDelete) => {
                if (willDelete) {
                    newsService.destroy(id).then(() => {
                        navigate('/news-and-events');
                    })

                    swal("Poof! Your item has been deleted!", {
                        icon: "success",
                    });
                } else {

                }
            });
    }


    return (
        <ThemeProvider theme={theme}>
            <Container component="main" maxWidth="lg">
                <CssBaseline />
                <DataGrid
                    columns={columns}
                    rows={data}
                    entity="news"
                    createUrl="/news-and-events/create"
                />
            </Container>
        </ThemeProvider>
    );
}